package rozproszone.klientserwer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;

public class TestSocket {
    public static void main(String[] args) {
        String address = "uek.krakow.pl";
        String path = "/";
        System.out.println("Pobieram dane ze strony: " + address);
        try {
            Socket socket = new Socket(address, 80);
            PrintStream out = new PrintStream(socket.getOutputStream());
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            out.print("GET " + path + " HTTP/1.0" + "\r\n");
            out.print("Host: " + address + "\r\n");
            out.print("\r\n");
            out.flush();
            String line = in.readLine();
            while (line != null) {
                System.out.println(line);
                line = in.readLine();
            }
            in.close();
            out.close();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}