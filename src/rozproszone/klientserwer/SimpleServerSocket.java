package rozproszone.klientserwer;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class SimpleServerSocket {
    public static void main(String[] args) {
        try {
            ServerSocket server = new ServerSocket(2345);
            while (true) {
                System.out.println("Oczekuje na połączenie...");
                Socket socket = server.accept();
                InetAddress address = socket.getInetAddress();
                System.out.println("Połączenie z adresu: " + address.getHostName() +
                        " [" + address.getHostAddress() + "]");
                Thread.sleep(10000);
                socket.close();
            }
        } catch (InterruptedException | IOException e) {
            e.printStackTrace();
        }
    }
}
