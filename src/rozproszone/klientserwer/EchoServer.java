package rozproszone.klientserwer;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class EchoServer {
    private static ServerSocket server;
    private static final int PORT = 5555;

    public static void main(String[] args) {
        String line;
        try {
            server = new ServerSocket(PORT);
            System.out.println("Serwer uruchomiony...");
            while (true) {
                Socket socket = server.accept();
                Scanner in = new Scanner(socket.getInputStream());
                PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
                out.println("Prosty serwer ECHO, komenda /end kończy działanie.");
                boolean done = false;
                while (!done) {
                    line = in.nextLine();
                    if (line.trim().toLowerCase().equals("/end")) {
                        done = true;
                    } else {
                        out.println(line);
                    }
                }
                socket.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (server != null) {
                try {
                    server.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
