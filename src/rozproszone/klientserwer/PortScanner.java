package rozproszone.klientserwer;

import java.io.IOException;
import java.net.Socket;

public class PortScanner {
    public static void main(String[] args) {
        Socket socket;
        String host = "uek.krakow.pl";
        for (int i = 0; i < 1024; i++) {
            try {
                socket = new Socket(host, i);
                System.out.println("Znalazłem serwer na porcie " + i +
                        " komputera: " + host);
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
