package rozproszone.klientserwer;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class EchoClient {
    static private final int PORT = 2345;
    static private final String SERVER = "localhost";
    static private final String PROMPT = "> ";

    public static void main(String[] args) {
        Scanner keyboard, in;
        PrintWriter out;
        Socket socket;
        try {
            System.out.println("Łącze się z serwerem na porcie " + PORT);
            socket = new Socket(SERVER, PORT);
            out = new PrintWriter(socket.getOutputStream(), true);
            in = new Scanner(socket.getInputStream());
            keyboard = new Scanner(System.in);
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        System.out.println(in.nextLine());

        boolean done = false;
        while (!done) {
            System.out.print(PROMPT);
            out.println(keyboard.nextLine());
            if (in.hasNextLine()) {
                System.out.println(in.nextLine());
            } else {
                done = true;
            }
        }

        System.out.println("Połączenie zostało zakończone.");

        try {
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
