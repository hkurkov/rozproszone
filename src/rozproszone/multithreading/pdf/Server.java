package rozproszone.multithreading.pdf;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

public class Server extends Thread {
    private ServerSocket server;

    private SimpleServerGui serverGui;

    public Server(SimpleServerGui serverGui) {
        this.serverGui = serverGui;
    }

    public void kill() {
        try {
            server.close();
            for (Connection client : serverGui.clients) {
                try {
                    client.out.println("Serwer przestał działać !");
                    client.socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            Connection.showMessage("Wszystkie Połączenia zostały zakończone.\n", serverGui);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void run() {
        try {
            server = new ServerSocket(new Integer(serverGui.portField.getText()));
            Connection.showMessage("Serwer uruchomiony na porcie: " + serverGui.portField.getText() + "\n", serverGui);
            while (serverGui.isRunning) {
                Socket socket = server.accept();
                Connection.showMessage("Nowe połączenie.\n", serverGui);
                new Connection(socket, serverGui).start();
            }
        } catch (SocketException e) {
            Connection.showMessage(e.toString(), serverGui);
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (server != null) server.close();
            } catch (IOException e) {
                Connection.showMessage(e.toString(), serverGui);
                e.printStackTrace();
            }
        }
        Connection.showMessage("Serwer zatrzymany.\n", serverGui);
    }
}
