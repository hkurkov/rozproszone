package rozproszone.multithreading.pdf;

import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class MultithreadedServer {
    private static final int PORT = 5555;

    public static void main(String[] args) {
        int connectionNum = 1;
        try {
            ServerSocket server = new ServerSocket(PORT);
            System.out.println("Serwer uruchomiony na porcie: " + PORT);
            while (true) {
                Socket socket = server.accept();
                InetAddress address = socket.getInetAddress();
                System.out.println("Połaczenie numer: " + connectionNum + " z adresu: " + address.getHostName() + " [" + address.getHostAddress() + "]");
                new Service(socket, connectionNum).start();
                connectionNum++;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
