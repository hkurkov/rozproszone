package rozproszone.multithreading.pdf;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

public class Service extends Thread {
    private Socket socket;
    private int connectionNum;

    public Service(Socket socket, int connectionNum) {
        this.socket = socket;
        this.connectionNum = connectionNum;
    }

    public void run() {
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintWriter out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()), true);
            out.println("Serwer wita użytkownika! Komenda /end kończy połączenie!");
            boolean done = false;
            while (!done) {
                String line = in.readLine();
                out.println("Połączenie: " + connectionNum + " Echo: " + line);
                if (line.trim().toLowerCase().equals("/end"))
                    done = true;
            }
            System.out.println("Połączenie numer: " + connectionNum + " zostało zakończone");
            socket.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
