package rozproszone.multithreading.pdf;

import java.io.*;
import java.net.Socket;

public class Connection extends Thread {
    public BufferedReader in;
    public PrintWriter out;
    public Socket socket;
    public String nick, line;

    public SimpleServerGui serverGui;

    public Connection(Socket socket, SimpleServerGui serverGui) {
        this.socket = socket;
        this.serverGui = serverGui;
        synchronized (this.serverGui.clients) {
            this.serverGui.clients.add(this);
        }
    }

    public void sendToAll(String tekst) {
        for (Connection client : serverGui.clients) {
            synchronized (client) {
                if (client != this)
                    client.out.println(tekst);
            }
        }
    }

    public void run() {
        try {
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()), true);
            out.println("Połączony z serwerem. Komenda /end kończy połączenie.");
            out.println("Podaj nick: ");
            nick = in.readLine();
            sendToAll("Użytkownik " + nick + " dołączył do czatu");
            while (serverGui.isRunning && !(line = in.readLine()).equalsIgnoreCase("/end")) {
                sendToAll("<" + nick + "> " + line);
            }
            out.println("Żegnaj.\n");
            synchronized (serverGui.clients) {
                serverGui.clients.remove(this);
            }
            sendToAll("Użytkownik " + nick + " opuścił czat.");
            showMessage("Połączenie zostało zakończone.\n", serverGui);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                in.close();
                out.close();
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void showMessage(String tekst, SimpleServerGui serverGui) {
        serverGui.messages.append(tekst);
        serverGui.messages.setCaretPosition(serverGui.messages.getDocument().getLength());
    }

}
