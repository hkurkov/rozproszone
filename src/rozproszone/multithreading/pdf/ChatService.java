package rozproszone.multithreading.pdf;

import java.io.*;
import java.net.Socket;
import java.util.Vector;

public class ChatService extends Thread {
    private static final Vector<ChatService> chats = new Vector<>();
    private Socket socket;
    private BufferedReader in;
    private PrintWriter out;
    private String nick;

    public ChatService(Socket socket) {
        this.socket = socket;
    }

    private void sendToAll(String msg) {
        for (ChatService chat : chats) {
            synchronized (chats) {
                if (chat != this) {
                    chat.out.println("<" + nick + "> " + msg);
                }
            }
        }
    }

    private void info() {
        out.print("Witaj " + nick + ", aktualnie czatują: ");
        for (ChatService chat : chats) {
            synchronized (chats) {
                if (chat != this) {
                    out.print(chat.nick + " ");
                }
            }
        }
        out.println();
    }

    public void run() {
        String line;
        synchronized (chats) {
            chats.add(this);
        }
        try {
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()), true);
            out.println("Połączony z serwerem. Komenda /end kończy połączenie.");
            out.print("Podaj swój nick: ");
            nick = in.readLine();
            System.out.println("Do czatu dołączył: " + nick);
            sendToAll("Pojawił się na czacie");
            info();
            while (!(line = in.readLine()).equalsIgnoreCase("/end")) {
                sendToAll(line);
            }
            sendToAll("Opuścił czat");
            System.out.println("Czat opuścił: " + nick);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                in.close();
                out.close();
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                synchronized (chats) {
                    chats.removeElement(this);
                }
            }
        }
    }
}