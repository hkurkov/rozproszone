package rozproszone.multithreading.pdf;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class ChatServer {
    private static final int PORT = 2345;

    public static void main(String[] args) {
        try {
            ServerSocket server = new ServerSocket(PORT);
            System.out.println("Czat Serwer uruchomiony na porcie: " + PORT);
            while (true) {
                Socket socket = server.accept();
                InetAddress address = socket.getInetAddress();
                System.out.println("Połączenie z adresu: " + address.getHostName() + " [" + address.getHostAddress() + "]");
                new ChatService(socket).start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
