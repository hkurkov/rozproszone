package rozproszone.multithreading.pdf;

import javax.swing.*;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

public class SimpleServerGui extends JFrame {
    public class ObslugaZdarzen implements ActionListener {
        private final SimpleServerGui simpleServerGui;
        private Server server;

        public ObslugaZdarzen(SimpleServerGui simpleServerGui) {
            this.simpleServerGui = simpleServerGui;
        }

        public void actionPerformed(ActionEvent e) {
            if (e.getActionCommand().equals("Uruchom")) {
                server = new Server(simpleServerGui);
                server.start();
                isRunning = true;
                start.setEnabled(false);
                stop.setEnabled(true);
                portField.setEnabled(false);
                repaint();
            }
            if (e.getActionCommand().equals("Zatrzymaj")) {
                server.kill();
                isRunning = false;
                stop.setEnabled(false);
                start.setEnabled(true);
                portField.setEnabled(true);
                repaint();
            }
        }
    }

    private JButton start, stop;
    private JPanel panel;
    public JTextField portField;
    public JTextArea messages;
    public int port = 5555;
    public boolean isRunning = false;
    public final Vector<Connection> clients = new Vector<>();

    public SimpleServerGui() {
        super("Czat Serwer");
        setSize(350, 300);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());
        panel = new JPanel(new FlowLayout());
        messages = new JTextArea();
        messages.setLineWrap(true);
        messages.setEditable(false);
        portField = new JTextField((new Integer(port)).toString(), 8);
        start = new JButton("Uruchom");
        stop = new JButton("Zatrzymaj");
        stop.setEnabled(false);
        ObslugaZdarzen event = new ObslugaZdarzen(this);
        start.addActionListener(event);
        stop.addActionListener(event);
        panel.add(new JLabel("Port: "));
        panel.add(portField);
        panel.add(start);
        panel.add(stop);
        add(panel, BorderLayout.NORTH);
        add(new JScrollPane(messages), BorderLayout.CENTER);
        setVisible(true);
    }

    public static void main(String[] args) {
        new SimpleServerGui();
    }
}
