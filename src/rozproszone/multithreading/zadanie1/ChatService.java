package rozproszone.multithreading.zadanie1;

import java.io.*;
import java.net.Socket;
import java.util.Vector;

public class ChatService extends Thread {
    private static final Vector<ChatService> chats = new Vector<>();
    private Socket socket;
    private BufferedReader in;
    private PrintWriter out;
    private String nick;

    public ChatService(Socket socket) {
        this.socket = socket;
    }

    private void sendToAll(String msg) {
        for (ChatService chat : chats) {
            synchronized (chats) {
                if (chat != this) {
                    chat.out.println("<" + nick + "> " + msg);
                }
            }
        }
    }

    private void info() {
        out.print("Witaj " + nick + ", aktualnie czatują: ");
        for (ChatService chat : chats) {
            synchronized (chats) {
                if (chat != this) {
                    out.print(chat.nick + " ");
                }
            }
        }
        out.println();
    }

    public String getNickFromCommand(String prefix, String line) {
        String[] tokens = line.split(" ");
        return tokens[0].replace(prefix, "");
    }

    private String dropSlashedFromNick(String nick) {
        if (nick.contains("/")) {
            return nick.replaceAll("/", "");
        }
        return nick;
    }

    private boolean getNick(String newNick) {
        if (newNick.length() < 1) {
            return false;
        }

        for (ChatService chat : chats) {
            if (chat == this) continue;

            if (newNick.equals(chat.nick)) {
                out.println("[SERVER] Nick " + newNick + " jest juz uzyty, wybierz inny");
                return false;
            }
        }

        return true;
    }

    @Override
    public void run() {
        String line;
        synchronized (chats) {
            chats.add(this);
        }
        try {
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()), true);

            out.println("Połączony z serwerem. Komenda /end kończy połączenie.");
            out.println("Podaj swój nick: ");
            out.print("> ");
            out.flush();

            String newNick;
            do {
                newNick = in.readLine();
                newNick = dropSlashedFromNick(newNick);
            } while (!getNick(newNick));
            this.nick = newNick;

            System.out.println("Do czatu dołączył: " + nick);
            sendToAll("Pojawił się na czacie");

            info();
            boolean isRunning = true;
            final String serverTag = "[SERVER]";
            while (isRunning) {
                out.print("> ");
                out.flush();

                line = in.readLine();

                System.out.println(line);

                if (line.startsWith("/")) {
                    String[] tokens = line.split(" ");
                    String command = tokens[0].toLowerCase();
                    if (command.equals("users")) {
                        StringBuilder users = new StringBuilder();
                        chats.forEach((chat) -> users.append(chat.nick).append(", "));
                        out.println(serverTag + " " + users.toString());
                    }

                    if (command.equals("end")) {
                        isRunning = false;
                    }

                    if (command.equals("ban")) {
                        String nick = tokens[1];

                        synchronized (chats) {
                            for (ChatService chat : chats) {
                                System.out.println(chat.nick);
                                if (chat.nick.equals(nick)) { // FIXME(hubert): usuwa poprawna osobe, ale zamyka zly socket, nie wiem dlaczego
                                    System.out.println(chat.nick + " should be removed");
                                    chats.removeElement(chat);
                                }
                            }
                        }

                        sendToAll(serverTag + " Banned " + nick);
                    }
                } else if (line.startsWith("@")) {
                    String nick = getNickFromCommand("@", line);
                    String message = line.replace("@" + nick, "");

                    ChatService user = null;
                    for (ChatService chat : chats) {
                        if (chat.nick.equals(nick)) {
                            user = chat;
                        }
                    }

                    if (user != null) {
                        user.out.println("[" + this.nick + "]" + censorLine(message));
                        System.out.println(user.nick + " found");
                    } else {
                        out.println(serverTag + " No users with that nickname " + nick);
                    }
                } else {
                    sendToAll(censorLine(line));
                }
            }
            sendToAll("Opuścił czat");
            System.out.println("Czat opuścił: " + nick);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                in.close();
                out.close();
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                synchronized (chats) {
                    chats.removeElement(this);
                }
            }
        }
    }

    private String censorLine(String line) {
        String[] bannedWords = {
                "dupa"
        };

        StringBuilder sentence = new StringBuilder();

        String[] tokens = line.split(" ");
        for (String word : tokens) {
            for (String bannedWord : bannedWords) {
                if (word.equals(bannedWord)) {
                    StringBuilder censoredWord = new StringBuilder();
                    for (int i = 0; i < word.length(); i++) {
                        censoredWord.append("*");
                    }
                    sentence.append(censoredWord);
                } else {
                    sentence.append(word).append(" ");
                }
            }
        }

        return sentence.toString();
    }
}