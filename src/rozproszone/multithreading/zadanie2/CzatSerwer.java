package rozproszone.multithreading.zadanie2;

import java.io.*;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Random;
import java.util.Vector;

public class CzatSerwer {
    private static ServerSocket server;
    private static final int PORT = 2345;

    public static void main(String[] args) {
        try {
            server = new ServerSocket(PORT);
            System.out.println("Czat Serwer uruchomiony na porcie: " + PORT);
            while (true) {
                Socket socket = server.accept();
                InetAddress addr = socket.getInetAddress();
                System.out.println("Połączenie z adresu: " + addr.getHostName()
                        + " [" + addr.getHostAddress() + "]");
                new CzatObsluga(socket).start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

class CzatObsluga extends Thread {
    private static final Vector<CzatObsluga> czaty = new Vector<>();
    private Socket socket;
    private BufferedReader in;
    private PrintWriter out;
    private String nick;

    public CzatObsluga(Socket socket) {
        this.socket = socket;
    }

    private void sendToAll(String tekst) {
        for (CzatObsluga czat : czaty) {
            synchronized (czaty) {
                if (czat != this)
                    czat.out.println("<" + nick + "> " + tekst);
            }
        }
    }

    private void info() {
        out.print("Witaj " + nick + ", aktualnie czatują: ");
        for (CzatObsluga czat : czaty) {
            synchronized (czaty) {
                if (czat != this)
                    out.print(czat.nick + " ");
            }
        }
        out.println();
    }

    private String dropSlashedFromNick(String nick) {
        if (nick.contains("/")) {
            return nick.replaceAll("/", "");
        }
        return nick;
    }

    private boolean getNick(String newNick) {
        if (newNick.length() < 1) {
            return false;
        }

        for (CzatObsluga chat : czaty) {
            if (chat == this) continue;

            if (newNick.equals(chat.nick)) {
                out.println("[SERVER] Nick " + newNick + " jest juz uzyty, wybierz inny");
                return false;
            }
        }

        return true;
    }

    public void run() {
        String line;
        synchronized (czaty) {
            czaty.add(this);
        }

        try {
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()), true);

            Random random = new Random();
            int guessNumber = random.nextInt(100 + 1);

            out.println("Połączony z serwerem. Komenda /end kończy połączenie.");
            out.println("Podaj swój nick: ");
            out.print("> ");
            out.flush();

            String newNick;
            do {
                newNick = in.readLine();
                newNick = dropSlashedFromNick(newNick);
            } while (!getNick(newNick));
            this.nick = newNick;

            sendToAll("Pojawił się na czacie");

            info();

            out.println("Jezeli chcesz zagrac w grę, to wpisz /game <liczba> gdzie <liczba> to jest od 1 do 100");

            boolean isRunning = true;
            final String serverTag = "[SERVER]";
            while (isRunning) {
                out.print("> ");
                out.flush();
                line = in.readLine();

                if (line.startsWith("/")) {
                    String[] tokens = line.split(" ");
                    String command = tokens[0].toLowerCase();

                    if (command.equals("game")) {
                        int param = Integer.parseInt(tokens[1]);

                        if (param > guessNumber) {
                            sendToAll(serverTag + " <" + this.nick + "> za duzo");
                        } else if (param < guessNumber) {
                            sendToAll(serverTag + " <" + this.nick + "> za malo");
                        } else {
                            sendToAll(serverTag + " <" + this.nick + "> zgadł");
                        }
                    }

                    if (command.equals("end")) {
                        isRunning = false;
                    }
                } else {
                    sendToAll(line);
                }
            }

            sendToAll("Opuścił czat");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                in.close();
                out.close();
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                synchronized (czaty) {
                    czaty.removeElement(this);
                }
            }
        }
    }
}