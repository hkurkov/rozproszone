package rozproszone.serializacja;

import java.io.*;

class Osoba implements Serializable {
    private String imie;
    private int wiek;
    private String plec;

    public Osoba(String imie, int wiek, String plec) {
        this.imie = imie;
        this.wiek = wiek;
        this.plec = plec;
    }

    public void wyswietlDane() {
        System.out.println("Osoba: " + imie + " ma lat: " + wiek + " i jest " + plec);
    }
}

public class TestSerializacji {
    public static void main(String[] a) {
        Osoba adam = new Osoba("Adam", 25, "mężczyzna");
        try {
            FileOutputStream out = new FileOutputStream("dane.dat");
            ObjectOutputStream oos = new ObjectOutputStream(out);
            oos.writeObject(adam);
            oos.close();
            System.out.println("Obiekt utworzony i zapisany.");
        } catch (IOException e) {
            System.out.println(e);
        }
        try {
            FileInputStream in = new FileInputStream("dane.dat");
            ObjectInputStream ois = new ObjectInputStream(in);
            Osoba osoba = (Osoba)ois.readObject();
            ois.close();
            System.out.println("Obiekt odczytany.");
            osoba.wyswietlDane();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}