package rozproszone.serializacja;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.*;

public class TestSerializacjiDoXML {
    public static void main(String[] args) throws IOException {
        Os adam = new Os();
        adam.setImie("Adam");
        adam.setWiek(23);
        adam.setPlec("mężczyzna");

        XMLEncoder xmle = new XMLEncoder(new BufferedOutputStream(new FileOutputStream("dane.xml")));
        xmle.writeObject(adam);
        xmle.close();
        System.out.println("Obiekt utworzony i zapisany.");

        XMLDecoder xmld = new XMLDecoder(new BufferedInputStream(new FileInputStream("dane.xml")));
        Os osoba = (Os)xmld.readObject();
        xmld.close();

        System.out.println("Obiekt odczytany.");
        osoba.wyswietlDane();
    }
}