package rozproszone.serializacja.wiadm;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Serwer {
    private static ServerSocket server;
    private static final int PORT = 2345;

    public static void main(String args[]) {
        try {
            server = new ServerSocket(PORT);
            System.out.println("Serwer uruchomiony...");
            while (true) {
                Socket socket = server.accept();
                ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
                oos.flush();

                ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
                Wiadomosc wiad = (Wiadomosc)ois.readObject();
                Wiadomosc odp = wiad.process();
                oos.writeObject(odp);

                ois.close();
                oos.close();
                socket.close();
            }
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            if (server != null) {
                try {
                    server.close();
                } catch (IOException ee) {
                    ee.printStackTrace();
                }
            }
        }
    }
}