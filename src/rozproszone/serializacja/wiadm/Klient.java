package rozproszone.serializacja.wiadm;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class Klient {
    static private Socket socket;
    static private final int PORT = 2345;
    static private final String SERVER = "localhost";

    public static void main(String[] args) {
        try {
            System.out.println("Łączę się z serwerem na porcie " + PORT);
            socket = new Socket(SERVER, PORT);
            System.out.println("Połączono.");

            ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
            ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
            oos.flush();
            Wiadomosc wiad = new Wiadomosc("Wysyłam informację do serwera.");

            oos.writeObject(wiad);
            oos.flush();

            Wiadomosc odp = (Wiadomosc)ois.readObject();
            System.out.println(odp);

            ois.close();
            oos.close();
            socket.close();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}