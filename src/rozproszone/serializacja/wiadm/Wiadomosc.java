package rozproszone.serializacja.wiadm;

import java.io.Serializable;

class Wiadomosc implements Serializable {
    private String tresc;

    public Wiadomosc(String tresc) {
        this.tresc = tresc;
    }

    public void setTresc(String tresc) {
        this.tresc = tresc;
    }

    public String getTresc() {
        return tresc;
    }

    public Wiadomosc process() {
        System.out.println("Otrzymałem wiadomość: " + tresc);
        return new Wiadomosc("Wszystko w porządku.");
    }

    public String toString() {
        return tresc;
    }
}