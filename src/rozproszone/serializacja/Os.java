package rozproszone.serializacja;

import java.io.Serializable;

public class Os implements Serializable {
    private String imie;
    private int wiek;
    private String plec;

    public Os() {

    }

    public void setImie(String i) {
        imie = i;
    }

    public String getImie() {
        return imie;
    }

    public void setWiek(int w) {
        wiek = w;
    }

    public int getWiek() {
        return wiek;
    }

    public void setPlec(String p) {
        plec = p;
    }

    public String getPlec() {
        return plec;
    }

    public void wyswietlDane() {
        System.out.println("Os: " + imie + " ma lat: " + wiek + " i jest " + plec);
    }
}